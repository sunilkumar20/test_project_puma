class CreateOrgActivities < ActiveRecord::Migration
  def change
    create_table :org_activities do |t|
      t.belongs_to :organization
      t.string :name
      t.integer :orgable_id
      t.string :orgable_type

      t.timestamps null: false
    end
  end
end
