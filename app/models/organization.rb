class Organization < ActiveRecord::Base
   
   uniq_id :name # it is defined in common_method.rb
   has_many :org_activities #, as: :orgable
   has_many :users, through: :org_activities, source: :orgable, source_type: 'User'
   has_many :projects, through: :org_activities, source: :orgable, source_type: 'Project'
  # has_many :projects   
  validates :name, presence: true
  
  def check_project_is_belongs_to_organization_or_user?(project, user)
    (user.projects.pluck(:id).include?(project.id) or self.projects.pluck(:id).include?(project.id)) rescue false
  end
end