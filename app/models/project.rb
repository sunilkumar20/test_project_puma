class Project < ActiveRecord::Base
  has_many :org_activities, as: :orgable, dependent: :destroy
  has_many :organizations, through: :org_activities
  has_many :project_activities, dependent: :destroy
  has_many :users, through: :project_activities, source: :projectable, source_type: 'User'
  #has_many :projects, through: :project_activities, source: :projectable, source_type: 'Project'
  #has_many :project_activities, as: :projectable
  #belongs_to :organizations
  accepts_nested_attributes_for :project_activities
  # validates :project_activities, presence: true
  validates :name, presence: true
  before_save :create_project_activity

  def create_project_activity
    
  end
end
