class ProjectActivity < ActiveRecord::Base
  include AASM
  belongs_to :project
  belongs_to :projectable, polymorphic: true
  belongs_to :user, class_name: "User", foreign_key: "projectable_id"
  before_save :set_data_for_users_asigned, if: :autocomplete_data_present?
  #before_update :check_already_assigned, if: Proc.new{ not self.autocomplete_user_id and not self.autocomplete_user_email }
  after_initialize :init
  
  aasm :column => :status do  # defaults to aasm_state
    state :assigned, :initial => true
    state :open
    state :pending
    state :confirm
    state :submitted
    state :re_assigned
    state :close
    state :created
    
    
    # event :assigned do
      # transitions :from => [:assigned], :to => [:open]
    # end
    event :open do 
      transitions :from => [:assigned, :open, :re_assigned], :to => :open
    end
    event :pending do
      transitions :from => [:assigned, :open, :re_assigned], :to => :pending
    end
    event :confirm do
      transitions :from => [:assigned, :open, :pending, :re_assigned, :confirm], :to => :confirm
    end
    event :submitted do
      transitions :from => [:assigned, :open, :pending, :confirm, :re_assigned], :to => :submitted
    end
    event :re_assigned do
      transitions :from => [:submitted], :to => :re_assigned
    end
    event :close do
      transitions :from => [:assigned, :open, :pending, :confirm, :submitted, :re_assigned], :to => :close
    end
  end
  
  def perform_event(event)
    begin
    case event
      when  "open"
        self.open!
      when  "pending"
        self.pending! 
      when "confirm"
        self.confirm!
      when "submitted"
        self.submitted!
      when "re_assigned"
        self.re_assigned!
      when "close"
        self.close!
    else  
    end
    rescue Exception => e   
      self.errors.add(:status, e.message)
    end
  end
  
  
  def init
    self.status ||= "assigned"
  end
  
  def autocomplete_data_present?
    self.autocomplete_user_id and self.autocomplete_user_email
  end
  
  def set_data_for_users_asigned
    user = User.find_user_by_id_and_email(self.autocomplete_user_id, self.autocomplete_user_email)
    assign_user_exists = user.project_activities.exists?(projectable_id: user.id, projectable_type: "User", status: "assigned", project_id: self.project_id)
    if user and not assign_user_exists
      self.projectable_id = user.id
      self.projectable_type = 'User'
    else
      self.errors.add(:autocomplete_user_email, "Already Exist") if assign_user_exists
      return false
    end
  end
  
  def check_already_assigned
    assign_user_exists = ProjectActivity.exists?(projectable_id: self.projectable_id, projectable_type: "User", status: "assigned", project_id: self.project_id)
    assign_user_exists ? (self.status = ProjectActivity.get_last_project_activity_of_an_project(self.user, self.project_id).status) : true
  end
  
  def self.get_last_project_activity_of_an_project(user, project_id)
    user.project_activities.where(project_id: project_id).order('id ASC').last
  end
  
  def abc
    
  end
  
end
