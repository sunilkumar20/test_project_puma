class DashboardController < ApplicationController
  
  def index
    @organizations = current_user.organizations
  end
  
  def new
    hash =  eval(params[:params_attr]) rescue {}
    project_id = params[:project_id].join(',') if params[:project_id].class == Array
    project_id = params[:project_id] if project_id.nil?
    path =  hash["project_id"] = project_id if hash["controller"] == "project_activities" and hash["project_id"].present? and hash["project_id"].present? and not params[:set_org].present?
    path = hash["id"] = current_organization.id if hash["controller"] == "organizations" and hash["id"].present? and params[:set_org].present?
       
    redirect_to hash.present? ? hash : dashboard_index_path
  end
  
end
