# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
20.times do 
  org = Organization.create(name: Faker::Company.name)
  rand(2..10).times do
    user = org.users.create(email: Faker::Internet.email, password: "password")
    # 5.times do
      # user.organizations.create(name: Faker::Company.name)
    # end
  end

  rand(1..10).times do
    no = rand(1..3)
    org.projects.create(name: Faker::Lorem.words(no).join(' '))
  end
end