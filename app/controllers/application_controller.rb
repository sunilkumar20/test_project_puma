class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
  before_filter :set_current_organization, :current_user_organizations, if: Proc.new{ current_user.present? }
  before_filter :check_organization_not_selected, if: Proc.new{not ["dashboard", "devise/sessions"].include?(params[:controller])}
  helper_method :current_organization, :redirect_url, :is_admin_of_project?
  
  def current_organization
    return organization = Organization.find_by_id(session[:current_org_id])
  end
 
  def set_current_organization
    if params[:controller] == "organizations"
      session[:current_org_id] = params[:id] if params[:id].present?
    elsif params[:organization_id].present?
      session[:current_org_id] ||= params[:organization_id]
    end
    
    if params[:organization_id].present? and session[:current_org_id] != params[:organization_id] and params[:set_org].present?
      if params[:organization_id].class == Array
        session[:current_org_id] = params[:organization_id].join(',')
      else
        session[:current_org_id] = params[:organization_id]
      end
    end
  end
  
  def current_user_organizations
    @organizations = current_user.organizations if current_user
    if @organizations.count == 1
      session[:current_org_id] = @organizations.first.id
    end
  end
  
  def check_organization_not_selected
    if @organizations.count == 1
      session[:current_org_id] = @organizations.first.id
    elsif not current_organization.present?
      flash[:alert] = "select org"
      redirect_to :back 
    end
  end

  def is_admin_of_project?(project)
    current_user.project_activities.where('project_id = ? and projectable_type = ? and status = ?', project.id, "User", 'created').present?
  end
  
  # def redirect_url
    # if (params[:organization_id] and params[:set_org].present? and not params[:check_page].present?)
     # return session[:redirect_url] = request.referrer
    # elsif params[:check_page].present?
      # return projects_path
    # end
  # end
  
  # def set_redirect_url
    # if (params[:controller] == "dashboard" and params[:action] == "new")
     # session[:redirect_url] = request.referrer
    # end
    # binding.pry
  # end
  
  def after_sign_in_path_for(resource_or_scope)
    if user_signed_in? and current_user
       dashboard_index_path
    else
      new_user_session_path
    end
  end
  
  def after_sign_out_path_for(resource_or_scope)
    if user_signed_in? and current_user
       dashboard_index_path
    else
      new_user_session_path
    end
  end
  
  
  def params_request_handled
    
    if params[:user_id].present?
      @projects = current_user.projects if current_user.id.to_s == params[:user_id]
      @projects = [] if not @projects.present?
    else
      @projects = current_organization.projects if current_user.check_user_role('admin')
      @projects = current_user.projects if not @projects.present?
    end
  end
  
  # helper_method :resource_name, :resource, :devise_mapping
# 
  # def resource_name
    # :user
  # end
#  
  # def resource
    # @resource ||= User.new
  # end
#  
  # def devise_mapping
    # @devise_mapping ||= Devise.mappings[:user]
  # end
end
