class CreateProjectActivities < ActiveRecord::Migration
  def change
    create_table :project_activities do |t|
      t.belongs_to :project
      t.integer :projectable_id
      t.string :projectable_type
      t.text :comment
      t.string :status 
      t.timestamps null: false
    end
  end
end
