class ProjectActivitiesController < ApplicationController
  before_filter :set_project
  before_filter :set_project_activity, only: [:show, :edit, :update, :destroy]
  
    
  def index
    if params[:assigned].present? and params[:assigned] == "true"
      @assigned_projects = current_user.projects
    else
      @project_activities = @project.project_activities rescue [] 
    end
     @project_activity = ProjectActivity.new
    
  end
  
  def new
    @project_activity = ProjectActivity.new
  end
  
  def edit
    
  end
  
  def update
    if params[:status].present?
      @project_activity.perform_event(params[:status].first)
    else
      if @project_activity.update(project_activities_params)
        redirect_to :back
      else
        render :edit
      end
    end
  end
  
  def create
    @project_activity  = @project.project_activities.new(project_activities_params)
    if @project_activity.save
      @project_activity.update_attributes(projectable: current_user) if params[:project_activity][:comment].present? or params[:project_activity][:status].present?
    end
  end
  
  def destroy
  end
  
  def show
  end
  
  private
    
  def set_project_activity
    @project_activity = ProjectActivity.find_by_id(params[:id])
  end
  
  def set_project
    @project = Project.find_by_id(params[:project_id]) if params[:project_id]
    if current_user.projects.count == 1 and @project.nil?
      @project = current_user.projects.last
    end
    if not @project or !(current_organization.check_project_is_belongs_to_organization_or_user?(@project, current_user) ) #or current_user.check_project_is_belongs_to_organization_or_user?(@project))
      params_request_handled
      render partial: "projects/select_project"
    end
  end
  
  def project_activities_params
    params.require(:project_activity).permit!
  end
  
end
