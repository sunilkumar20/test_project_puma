class ProjectsController < ApplicationController
  
  before_filter :set_project, only: [:show, :edit, :update, :destroy]
  before_filter :params_request_handled, only: [:index]

  def index
  end
  
  def new
    @project = Project.new
  end
  
  def edit
    
  end
  
  def update
    if @project.update(project_params)
      redirect_to projects_path
    else
      render :edit
    end
  end
  
  def create
    @project = current_organization.projects.create(project_params)
    @project.project_activities.create(projectable_id: current_user.id, projectable_type: "User", status: 'created')
    if not @project.errors.present?
      redirect_to projects_path
    else
      render :new
    end
  end
  
  def destroy
    redirect_to :back if @project.destroy
  end
  
  def show
    @project_activity = @project.project_activities.build
  end
  
  private
    
  def set_project
    @project = current_organization.projects.find_by_id(params[:id]) if params[:id].present?
    if not @project or not current_organization.projects.pluck(:id).include?(@project.id)
      redirect_to projects_path
    end
  end
  
  def project_params
    params.require(:project).permit!
  end
  
    
  # def params_request_handled
#     
    # if params[:user_id].present?
      # @projects = current_user.projects if current_user.id.to_s == params[:user_id]
      # @projects = [] if not @projects.present?
    # else
      # @projects = current_organization.projects if current_user.check_user_role('admin')
      # @projects = current_user.projects if not @projects.present?
    # end
  # end
end
