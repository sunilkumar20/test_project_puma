class User < ActiveRecord::Base
  attr_accessor :user_email
  resourcify
  rolify
  #uniq_id 
  #include SessionData
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :org_activities, as: :orgable
  has_many :organizations, through: :org_activities #, source_type: 'User'
  has_many :project_activities, as: :projectable
  has_many :projects, through: :project_activities
  #has_many :org_users, through: :organizations, source: :orgable, source_type: 'User'
  
  #has_many :project_activities, as: :projectable
  #has_and_belongs_to_many  :org_activities
  def self.find_user_by_id_and_email(id, email)
     find_by_id_and_email(id, email)
  end
  
  def organizations_users
    org_ids = self.organizations.pluck(:id) 
    User.joins(:org_activities, :organizations).where(org_activities: {organization_id: org_ids}).group(:id)
  end
  
  def check_user_role(role)
    self.roles.pluck(:name).include?(role)
  end
  
  private
  def password_required?
    false #new_record? ? super : false
  end  
end
