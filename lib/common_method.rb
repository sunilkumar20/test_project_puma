
module CommonMethod
  extend ActiveSupport::Concern
  attr_accessor :uniq_id
  attr_accessor :autocomplete_user_id, :autocomplete_user_email
  
  def self.included(base)
    base.extend(ClassMethods)
  end
  
  module ClassMethods
    def uniq_id(id)
      @uniq_id = id 
      # define_method :"self.find_by_#{@uniq_id.to_s}" do
        # binding.pry
        # puts "I am method #{@uniq_id} -- #{id}!"
      # end
      # class << self
          # define_method "find_by_id" do
          # self.send("find")
        # end #define_method
      # end #class << self
    end
   
    def get_uniq_id
      @uniq_id
    end
    
    # def find_by_id(id)
       # p "hhhiiiiiiiii"
       # super
#        
    # end
#     
    # def find(id)
       # p "hhh"
       # super
    # end
    
    # def encrypt_ids(arg)
    # end
  end
  
  module CommonMethodForParamId
   def to_param
    attributes = self.class.column_names
    if self.class.get_uniq_id
      ("#{self.id}-#{self.attributes[self.class.get_uniq_id.to_s]}")
    else
       "#{self.id}-#{self.name}" rescue self.id.to_s
    end
   end
  end
end

ActiveRecord::Base.send(:include, CommonMethod::CommonMethodForParamId)
