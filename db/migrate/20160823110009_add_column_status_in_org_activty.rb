class AddColumnStatusInOrgActivty < ActiveRecord::Migration
  def change
  	add_column :org_activities, :status , :string
  	add_column :org_activities, :user_id, :integer
  end
end
