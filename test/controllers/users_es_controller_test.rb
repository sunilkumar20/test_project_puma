require 'test_helper'

class UsersEsControllerTest < ActionController::TestCase
  setup do
    @users_e = users_es(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users_es)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create users_e" do
    assert_difference('UsersE.count') do
      post :create, users_e: {  }
    end

    assert_redirected_to users_e_path(assigns(:users_e))
  end

  test "should show users_e" do
    get :show, id: @users_e
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @users_e
    assert_response :success
  end

  test "should update users_e" do
    patch :update, id: @users_e, users_e: {  }
    assert_redirected_to users_e_path(assigns(:users_e))
  end

  test "should destroy users_e" do
    assert_difference('UsersE.count', -1) do
      delete :destroy, id: @users_e
    end

    assert_redirected_to users_es_path
  end
end
