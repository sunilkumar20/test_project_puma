class OrganizationsController < ApplicationController
  before_filter :set_organization, only: [:show, :edit, :update, :destroy]
  
  def index
    #@organizations = current_user.organizations
    @users = current_organization.users
    @projects = current_organization.projects
  end
  
  def new
    @organization = Organization.new
  end
  
  def edit
    @organization = Organization.find_by_id(params[:id])
  end
  
  def update
    @organization.update(organization_params)
    redirect_to @organization
  end
  
  def create
    @organization = current_user.organizations.create(organization_params)
    if not @organization.errors.present?
      redirect_to organizations_path
    else
      render :new
    end
  end
  
  def show
  end
  
  def destroy
    
  end
  
  private
  
  def set_organization
    @organization = (Organization.find_by_id(params[:id]) || current_organization)
  end
  
  def organization_params
    params.require(:organization).permit!
  end
  
end
